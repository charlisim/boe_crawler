import sqlite3
from settings import *
from urls import *

class bbdd_url():
	def __init__(self):
		self.conn = sqlite3.connect(DB_FILE)
		self.c = self.conn.cursor()

		
	
	def createTable(self):
		self.c.execute('create table url_days(id integer primary key unique, url text unique , checked integer)')
		self.c.execute('create table urls(id integer primary key unique, url text unique , checked integer)')
		self.conn.commit()
	def saveQueue(self, queue):
		
		self.c.execute('INSERT OR REPLACE INTO urls (url, checked) VALUES (?,?)', (queue, 0))		
		self.conn.commit()

	def saveDays(self, queue):
		
		try:
			self.c.executemany('INSERT INTO url_days (url, checked) VALUES (?,?)', queue)		
			self.conn.commit()
			pass
		except sqlite3.IntegrityError:
			print 'Ya estan'
	def getDays(self):
		
			self.c.execute('SELECT url FROM url_days WHERE checked = 0')
			return self.c.fetchall()

			
		
	def setCheckURL(self, url):

		self.c.execute('REPLACE INTO url_days (id, url, checked) VALUES( (select id from url_days where url = (?)),(?), 1)', (url, url))
		self.conn.commit()
		print url
		print 'YA ESTA 1'
	def checkURL(self, URL):
		
		self.c.execute('SELECT * FROM urls WHERE url = (?)', (URL[0]))
		if self.c.fetchone():
			pass
		else:
			return False
	
	def close(self):
		self.conn.close()
